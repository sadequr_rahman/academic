#include "priorityqueue.h"

PriorityQueue::PriorityQueue(int maximum_priority)
{
    head = NULL;
    max_priority = maximum_priority;
}

PriorityQueue::~PriorityQueue()
{
    clear();
}

void PriorityQueue::clear()
{
    if(head != NULL)
    {
        Node *curr = head;
        Node *next;

        while(curr != NULL)
        {
            next = curr->next;
            delete curr;
            curr = next;
        }

        head = NULL;
    }
}

Node *PriorityQueue::top()
{
    return head;
}

bool PriorityQueue::push(const int priority, const string data)
{
    if(priority > this->max_priority)
    {
        cout << "Error: priority level must be an integer between 1-" << this->max_priority << endl;
        return false;
    }
    else if(head == NULL)
    {
        Node *item = new Node;
        item->priority = priority;
        item->data = data;
        item->next = NULL;
        head = item;
        return true;
    }
    else if(top()->priority < priority)
    {
        Node *item = new Node;
        item->priority = priority;
        item->data = data;
        item->next = head;
        head = item;
        return true;
    }
    else
    {
        Node *item = new Node;
        item->priority = priority;
        item->data = data;
        item->next = NULL;

        Node *curr = head;
        Node *prev = NULL;

        while(curr != NULL)
        {
            if(item->priority > curr->priority)
            {
                item->next = curr;
                prev->next = item;
                return true;
            }

            prev = curr;
            curr = curr->next;
        }

        prev->next = item;

        return true;
    }
}

bool PriorityQueue::pop()
{
    if(head == NULL)
    {
        return false;
    }
    else
    {
        Node *item = head->next;
        delete head;
        head = item;
        return true;
    }
}

bool PriorityQueue::erase(const int number)
{

    Node *curr = head;
    Node *pre = NULL;
    int running_number = 0;
    while (curr != NULL)
    {
        running_number++;

        if(running_number == number)
        {
            if(pre == NULL)
            {
                Node *item = NULL;
                item = curr->next;
                delete curr;
                head = item;
            }
            else
            {
                Node *item = NULL;
                item = curr->next;
                delete curr;
                pre->next = item;
            }
            return true;
        }
        pre = curr;
        curr = curr->next;
    }
    return false;
}

ostream& operator << (ostream& out, const PriorityQueue &pq)
{
    if(pq.head == NULL)
    {
        return out;
    }
    else
    {
        Node *curr = pq.head;

        int inuse = INT_MAX;
        int running_number = 1;

        while(curr != NULL)
        {
            if(inuse != curr->priority)
            {
                inuse = curr->priority;
                cout << "Priority level " << inuse << ":" << endl;
            }

            out << "  " << running_number /*<< " [" << curr->priority << "] "*/
                << " " << curr->data
                << " " << endl;

            curr = curr->next;
            running_number++;
        }

        return out;
    }
}
