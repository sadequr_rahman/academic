#include <iostream>
#include <cstdlib>
#include "priorityqueue.h"

using namespace std;

int main()
{
    PriorityQueue pq;
    pq.max_priority = 5;

    pq.push(1, "pay the bill");
    pq.push(4, "play games");
    pq.push(5, "have a party");
    pq.push(1, "wash the dishes");
    pq.push(1, "vacuum clean the house");
    pq.push(3, "feed the goldfish");
    pq.push(2, "fix bike tire");

    cout << pq << endl;
    pq.erase(7);
    cout << pq << endl;



    return 0;
}

