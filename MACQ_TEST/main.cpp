#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;

class MacqSample
{
public:
	MacqSample()
	{
		macqBuf = new float(macqSize);
		memset(macqBuf,0.00,macqSize * sizeof(float));
	};
	~MacqSample()
	{
		delete[] macqBuf;
	}

	void put(float _val)
	{
		for (int i = (macqSize-1); i > 0; --i)
		{
			macqBuf[i] = macqBuf[i-1];
		}

		macqBuf[0] = _val;
	};

	float averageDeviation(void)
	{
		float avg = 0;
		for (int i = 0; i < macqSize-1; ++i)
		{
			avg += std::abs(macqBuf[i] - macqBuf[i+1]);
		}
		avg = avg / (macqSize-1);

		return avg;
	};

	float standerDeviation(void)
	{
	    float u = average();
	    float standardDeviation = 0;
	    for(int i = 0; i < macqSize; ++i)
            standardDeviation += pow(macqBuf[i] - u, 2);

        return sqrt(standardDeviation / macqSize);
	};

	float recentDeviation(void)
	{
		return std::abs(macqBuf[0] - macqBuf[1]);

	};

	void toString()
	{
	    for(int i=0;i<macqSize;++i)
        {
            cout << "[" <<macqBuf[i] << "] " ;
        }
        cout << endl;
	}

	float average(void)
	{
	    float avg = 0.00;
		for (int i = 0; i < macqSize; ++i)
		{
			avg += macqBuf[i];
		}
		avg = avg / macqSize;

		return avg;
	}

public:
	float *macqBuf;
	const int macqSize = 4;

};

int main()
{
    MacqSample macq;
    macq.toString();
    macq.put(4.00);
    macq.toString();
    macq.put(15.02);
    macq.toString();
    macq.put(20.025);
    macq.toString();
    macq.put(6.5);
    macq.toString();
    cout << "Average Deviation: " << macq.averageDeviation() << endl;
    cout << "Current Deviation: " << macq.recentDeviation() << endl;
    cout << "Standard Deviation: " << macq.standerDeviation() << endl;



    return 0;
}
