#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include <stdexcept>

void writeTOFile(std::string& msg){
    // mutex to protect file access (shared across threads)
    static std::mutex mutex;
    // lock mutex before accessing file
    std::lock_guard<std::mutex> lock(mutex);
    // try to open file
    std::ofstream file("example.txt");
    if (!file.is_open())
        throw std::runtime_error("unable to open file");

    // write message to file
    file << msg << std::endl;

    // file will be closed 1st when leaving scope (regardless of exception)
    // mutex will be unlocked 2nd (from lock destructor) when leaving
    // scope (regardless of exception)
}


int main()
{
    std::string str = "Hello RaIi";
    std::cout << "Hello world!" << std::endl;
    writeTOFile(str);
    return 0;
}
