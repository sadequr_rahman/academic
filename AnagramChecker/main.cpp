#include <iostream>
#include <string>
#include <conio.h>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

static const int maxSizeOfWord(20);
static const int totalNumInputChar(2000);
static int totalInput(0);

bool readValidLine(string& str);
bool isAllCharExist(const string& src,const string& dest);
int charAt(const string& src, const char c);


int main()
{
    std::vector<string>dictionary;
    std::vector<string>phases;
    std::vector<string>result;
    string test;
    while(totalInput < totalNumInputChar){
        if(readValidLine(test)){
            if(test == "#\r"){
                break;
            }else{
                    dictionary.push_back(test);
                    totalInput += test.size() -1 ;
                    test = "";
                }
        }else{
            break;
            }
    }

    test = "";
    while(totalInput < totalNumInputChar){
        if(readValidLine(test)){
                if(test == "#\r"){
                        break;
                }else{
                        phases.push_back(test);
                        totalInput += test.size() -1 ;
                        test = "";
                        }
                }else{
                    break;
                }
        }

    cout << "Dictionary Inputs are -> " << endl;
    for(vector<string>::iterator it = dictionary.begin(); it != dictionary.end(); it++)
		cout << *it << endl;
    cout << "Phase Inputs are -> " << endl;
    for(vector<string>::iterator it = phases.begin(); it != phases.end(); it++)
		cout  << *it <<endl;
    cout << "total Input char is -> " << totalInput << endl;


    /* TO-DO output block is not working as expected */
    test = "";
    for(uint16_t i = 0; i < phases.size(); ++i){
        cout << phases[i] <<  " = " ;
        for(uint16_t j=0;j<dictionary.size();++j){
            if(isAllCharExist(phases[i],dictionary[j])){
                    cout << dictionary[j] << ' ';

            }
        }
        cout << endl;
    }

    return 0;
}



int charAt(const string& src, const char c){
        for(unsigned int i = 0; i < src.length();++i){
            if(src[i] == c){
                return i;
            }
        }
    return -1;
}

bool isAllCharExist(const string& src,const string& dest){
    for(unsigned int i = 0; i < dest.length(); ++i){
        //cout << "Destination ["<< i <<"] " << dest[i] << endl;
        if(charAt(src,dest[i]) == -1){
            return false;
        }

    }

    return true;
}


bool readValidLine(string& str){
    char t;
    int latterCount(0);
    do{
        t =_getche();
        str += t;
        if(t == '\r'){
            //cout << "Read Line Successful" <<endl;
            cout << str <<endl;
            return true;
        }else if(!isupper(t) && !(t =='#') && !(t==' ')){
            cout << "error: Input is not upper case" <<endl;
            return false;
        }
        else{
            latterCount++;
        }
    }while(latterCount < maxSizeOfWord);
    cout << "error: Input out of max limit " <<endl;
    return false;
}
